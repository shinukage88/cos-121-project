#include <cstdlib>
#include "ShortFactory.h"
#include "TallFactory.h"
#include "Minnakontralesky.h"
#include "Ability.h"

using namespace std;

int main() 
{
    TallFactory tallFact;
    ShortFactory shortFact;
    
    Minion* one = tallFact.createOneEyed();
    Minion* two = tallFact.createTwoEyed();
    Minion* three = shortFact.createOneEyed();
    Minion* four = shortFact.createTwoEyed();
    
    cout << "Minion 1: "; one->print();
    cout << "Minion 2: "; two->print();
    cout << "Minion 3: "; three->print();
    cout << "Minion 4: "; four->print();
    
    cout << endl;
    
    cout << "Copying Minion 1 to Minion 5..." << endl;
    Minion* five = one->clone();
    
    cout << "Minion 5: "; five->print();
    
    
    cout << endl;
    
    cout << "Creating Minnakontralesky...." << endl;
    Minion* minna = new Minnakontralesky();
    
    cout << "Minna: "; minna->print();
    
    cout << endl;
    cout << "Attempting to clone Minnakontralesky...." << endl;;
    Minion* minnaClone = minna->clone();
    
    cout << "MinnaClone: "; minnaClone->print();
    
    Ability* gunsmith = new Ability("Gunsmith");
    Ability* smelter = new Ability("Smelter");
    Ability* tinkerer = new Ability("Tinkerer");
    Ability* chemist = new Ability("Chemist");
    cout << endl << endl;
    cout << "Adding Abilities...." << endl;
    one->addAbility(gunsmith);
    cout << "Minion 1: "; one->print();
    
    cout << endl;
    one->addAbility(tinkerer);
    cout << "Minion 1: "; one->print();
    
    
    minna->addAbility(gunsmith);
    cout << "Minna: "; minna->print();

    return 0;
}