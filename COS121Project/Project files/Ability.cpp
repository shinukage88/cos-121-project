#include "Ability.h"

Ability::Ability() 
{
    mAbility = "None";
}

Ability::Ability(string ability)
{
    mAbility = ability;
}

Ability::~Ability() 
{
}

string Ability::getAbility()
{
    return mAbility;
}

