#include "Minion.h"
#include "ShortOne.h"

Minion::Minion(string type)
{
	mType = type;
        mAbility = "None";
}

Minion* Minion::clone()
{   
       cout << "Minnakontralesky cannot be cloned! Short One Eyed minion created instead...." << endl;
       return new ShortOne();
}

void Minion::addAbility(Ability* ability)
{
    if (mType == "Minnakontralesky")
    {
        mAbility = "Special";
        cout << "Minnakontralesky given 'Special' ability..." << endl;;
    }
    else
    if (mAbility == "None")
    {
        mAbility = ability->getAbility();
    }
    else
    {
        cout << "Minion already has an ability." << endl;
    }
}

void Minion::print()
{
    cout << "Type: " << mType << " with the ability: " << mAbility << endl;
}