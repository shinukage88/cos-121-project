#ifndef WEAPONCREATION_H
#define	WEAPONCREATION_H

#include "WhitePhosphorus.h"
#include "BubonicGrenade.h"
#include "UraniumSword.h"

class WeaponCreation
{
public:
    WeaponCreation();
private:
    Weapon* mWhitePhosphorus;
    Weapon* mBubonicGrenade;
    Weapon* mUraniumSword;
};

#endif	/* WEAPONCREATION_H */

