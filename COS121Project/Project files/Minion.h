#ifndef MINION_H
#define MINION_H

#include<string>
#include<iostream>
#include "Ability.h"

using namespace std;

class Minion
{
	private:
		string mType;
                string mAbility;
	public:
		Minion(string type);
                virtual Minion* clone();
                void addAbility(Ability*);
                
                void print();
};

#endif