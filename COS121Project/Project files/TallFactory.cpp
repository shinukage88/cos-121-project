#include "TallFactory.h"
#include "TallOne.h"

Minion* TallFactory::createOneEyed()
{
    return new TallOne();
}
Minion* TallFactory::createTwoEyed()
{
    return new TallTwo();
}
