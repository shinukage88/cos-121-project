#ifndef ABILITY_H
#define	ABILITY_H

#include <string>
using namespace std;

class Ability {
public:
    Ability();
    Ability(string);
    virtual ~Ability();
    string getAbility();
    
private:
    string mAbility;

};

#endif

