#ifndef SHORTFACTORY_H
#define SHORTFACTORY_H

#include "MinionFactory.h"
#include "Minion.h"
#include "ShortOne.h"
#include "ShortTwo.h"

class ShortFactory : public MinionFactory
{
	public:
		Minion* createOneEyed();
		Minion* createTwoEyed();
};

#endif