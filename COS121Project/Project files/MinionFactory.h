#ifndef MINIONFACTORY_H
#define MINIONFACTORY_H

#include "Minion.h"

class MinionFactory
{
	public:
		virtual Minion* createOneEyed() = 0;
		virtual Minion* createTwoEyed() = 0;
};

#endif