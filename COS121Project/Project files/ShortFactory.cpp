#include "ShortFactory.h"

Minion* ShortFactory::createOneEyed()
{
    return new ShortOne();
}

Minion* ShortFactory::createTwoEyed()
{
    return new ShortTwo();
}