#ifndef TALLFACTORY_H
#define TALLFACTORY_H

#include "MinionFactory.h"
#include "Minion.h"
#include "ShortTwo.h"
#include "TallTwo.h"

class TallFactory : public MinionFactory
{
	public:
		Minion* createOneEyed();
		Minion* createTwoEyed();
};

#endif