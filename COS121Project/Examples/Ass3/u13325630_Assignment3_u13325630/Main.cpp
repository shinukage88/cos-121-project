#include "Creator.h"
#include <time.h>
#include <stdlib.h>
#include <iostream>

int main()
{
	cout << "MONSTER BATTLE - ARGGGGGHHHHH!" << endl;
	cout << "---------------------------------------------" << endl;

	// For random numbers
	srand(time(NULL));

	// Create the factories
	ArcherCreator archCreator;
	KnightCreator knightCreator;
	MageCreator mageCreator;
	ZombieCreator zombieCreator;
	SkeletonCreator skeletonCreator;

	// Create the players
	const int UNIT_COUNT = 3;
	Unit **units = new Unit*[UNIT_COUNT];
	units[0] = archCreator.create();
	units[1] = knightCreator.create();
	units[2] = mageCreator.create();

	// Create the monsters
	const int MONSTER_COUNT = 10;
	Monster *prototype1 = zombieCreator.create();
	Monster *prototype2 = skeletonCreator.create();
	Monster* monsters[MONSTER_COUNT];
	for(int i = 0; i < MONSTER_COUNT; ++i)
	{
		if(rand() % 2) monsters[i] = prototype1->clone();
		else monsters[i] = prototype2->clone();
	}
	
	// Play
	for(int i = 0 ; i < MONSTER_COUNT; ++i) units[rand() % UNIT_COUNT]->attack(monsters[i]);

	// Determine winner
	int deadCounter = 0;
	for(int i = 0; i < UNIT_COUNT; ++i)
	{
		if(units[i]->isDead()) ++deadCounter;
	}
	cout << "---------------------------------------------" << endl;
	if(deadCounter == UNIT_COUNT) cout << "All players were killed by the monsters. You lose!" << endl;
	else cout << "You survived all monsters. You win!" << endl;
	
	// Clear memory
	for(int i = 0; i < UNIT_COUNT; ++i) delete units[i];
	delete [] units;

	for(int i = 0; i < MONSTER_COUNT; ++i) delete monsters[i];
	delete prototype1;
	delete prototype2;
	
	return 0;
}
