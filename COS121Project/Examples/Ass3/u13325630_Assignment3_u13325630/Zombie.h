#ifndef ZOMBIE_H
#define ZOMBIE_H

#include "Monster.h"

class Zombie : public Monster
{
	public:
            Zombie();
            Monster* clone();
		
};

#endif