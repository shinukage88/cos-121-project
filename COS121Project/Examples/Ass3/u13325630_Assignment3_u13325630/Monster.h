#ifndef MONSTER_H
#define MONSTER_H

#include <stdio.h>     
#include <stdlib.h>     
#include <time.h> 
#include "Unit.h"

class Monster : public Unit
{
    private:
        int mMinimum;
        int mMaximum;
        
    public:
        Monster(string type, int health, int damage, int minimum, int maximum);
        Monster(Monster& other);
        void takeDamage(int damage);
        virtual Monster* clone() = 0;
};

#endif