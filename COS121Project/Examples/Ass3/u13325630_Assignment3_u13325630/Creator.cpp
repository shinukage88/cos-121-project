#include "Creator.h"

Mage* MageCreator::create()
{
    return new Mage();
}

Archer* ArcherCreator::create()
{
    return new Archer();
}

Knight* KnightCreator::create()
{
    return new Knight();
}
Zombie* ZombieCreator::create()
{
    return new Zombie();
}
Skeleton* SkeletonCreator::create()
{
    return new Skeleton();
}