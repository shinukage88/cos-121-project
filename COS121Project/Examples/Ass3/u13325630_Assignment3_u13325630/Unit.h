#ifndef UNIT_H
#define UNIT_H

#include<string>
#include<iostream>
using namespace std;

class Unit
{
	private:
		string mType;
		int mHealth;
		int mDamage;
	public:
		Unit(string type, int health, int damage);
		Unit(Unit & other);
		int damage();
		void attack(Unit* enemy);
		void die();
		bool isDead();
		void takeDamage(int damage);
};

#endif