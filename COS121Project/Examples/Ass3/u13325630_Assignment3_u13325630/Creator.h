#ifndef CREATOR_H
#define CREATOR_H

#include "Archer.h"
#include "Knight.h"
#include "Mage.h"
#include "Unit.h"
#include "Zombie.h"
#include "Skeleton.h"

class Creator
{
	public:
		virtual Unit* create() = 0;
};

class KnightCreator: public Creator
{
public: 
    Knight* create();
};

class ArcherCreator: public Creator
{
    public: 
    Archer* create();
};

class MageCreator: public Creator
{
    public: 
    Mage* create();
};

class SkeletonCreator: public Creator
{
    public: 
    Skeleton* create();
};

class ZombieCreator: public Creator
{
    public: 
    Zombie* create();
};

#endif