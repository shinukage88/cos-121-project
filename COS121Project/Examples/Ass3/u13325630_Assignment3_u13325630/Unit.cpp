#include "Unit.h"

Unit::Unit(string type, int health, int damage)
{
    mType = type;
    mHealth = health;;
    mDamage = damage;
}
Unit::Unit(Unit & other)
{
    mType = other.mType;
    mHealth = other.mHealth;
    mDamage = other.mDamage;
}
int Unit::damage()
{
    return mDamage;
}
void Unit::attack(Unit* enemy)
{
    if(isDead() == false)
    {
        enemy->takeDamage(mDamage);
        takeDamage(enemy->damage());
    }
}
void Unit::die()
{
    cout << "The " << mType << " was killed." << endl;
}
bool Unit::isDead()
{
    if(mHealth <= 0)
        return true;
    else 
        return false;
}
void Unit::takeDamage(int damage)
{
/*This function should only do something if the unit is not dead. The function should reduce
the unit’s health with the passed damage parameter and reset the health to zero if it drops below zero. A
message containing the type, damage caused and the new health, should then be printed. If the health
reduction causes the unit to die, the die function should be called*/
    if(isDead() == false)
    {
        mHealth = mHealth - damage;
        if(isDead())
        {
            mHealth = 0;
        }
        cout << "The " << mType <<  " took " << damage << " damage ( " << mHealth << " remaining)." << endl;
        if(isDead())
        {
            die();
        }
    }
}