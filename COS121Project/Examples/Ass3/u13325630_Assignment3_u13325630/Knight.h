#ifndef KNIGHT_H
#define KNIGHT_H

#include"Unit.h"

class Knight : public Unit
{
	public:
		Knight();
                void takeDamage(int damage);
};

#endif