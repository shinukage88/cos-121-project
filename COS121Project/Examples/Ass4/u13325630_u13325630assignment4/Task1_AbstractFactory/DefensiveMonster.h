#ifndef DEFENSIVEMONSTER_H
#define DEFENSIVEMONSTER_H

#include"Monster.h"

class DefensiveMonster : public Monster
{
	public:
		DefensiveMonster();
                void attack(Unit * enemy);
};

#endif