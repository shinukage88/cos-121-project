#ifndef OFFENSIVEKNIGHT_H
#define OFFENSIVEKNIGHT_H

#include"Knight.h"

class OffensiveKnight : public Knight
{
	public:
		OffensiveKnight();
                void attack(Unit * enemy);
};

#endif