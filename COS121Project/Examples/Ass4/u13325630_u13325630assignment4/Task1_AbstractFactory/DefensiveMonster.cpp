#include "DefensiveMonster.h"

DefensiveMonster::DefensiveMonster(): Monster("Defensive Monster", 80, 40, 10, 20)
{
	
}
void DefensiveMonster::attack(Unit * enemy)
{
	if(isDead() == false)
	{
        enemy->takeDamage(mDamage);
        takeDamage(enemy->damage());
	}
	if(enemy->isDead() == true)
	{
		mDamage += 1;
		mHealth += 2;
	}
}