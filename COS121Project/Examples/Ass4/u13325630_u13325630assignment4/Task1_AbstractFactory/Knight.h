#ifndef KNIGHT_H
#define KNIGHT_H

#include"Unit.h"

class Knight : public Unit
{
	public:
		Knight(string type, int health, int damage);
                virtual void takeDamage(int damage);
};

#endif