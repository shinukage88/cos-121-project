#include "DefensiveFactory.h"

Unit*DefensiveFactory::createMonster()
{
	return new DefensiveMonster();
}
Unit*DefensiveFactory::createKnight()
{
	return new DefensiveKnight();
}