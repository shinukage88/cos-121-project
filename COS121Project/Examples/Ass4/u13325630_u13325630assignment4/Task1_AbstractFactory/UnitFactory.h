#ifndef UNITFACTORY_H
#define UNITFACTORY_H

#include "Unit.h"

class UnitFactory
{
	public:
		virtual Unit*createMonster() = 0;
		virtual Unit*createKnight() = 0;
};

#endif