#ifndef MONSTER_H
#define MONSTER_H

#include <stdio.h>     
#include <stdlib.h>     
#include <time.h> 
#include "Unit.h"

class Monster : public Unit
{
    private:
        int mMinimum;
        int mMaximum;
        
    public:
        Monster(string type, int health, int damage, int minimum, int maximum);
        virtual void takeDamage(int damage);
};

#endif