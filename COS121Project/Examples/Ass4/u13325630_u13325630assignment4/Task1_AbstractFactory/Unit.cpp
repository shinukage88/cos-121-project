#include "Unit.h"

Unit::Unit(string type, int health, int damage)
{
    mType = type;
    mHealth = health;;
    mDamage = damage;
}
int Unit::damage()
{
    return mDamage;
}
void Unit::attack(Unit* enemy)
{
    if(isDead() == false)
    {
        enemy->takeDamage(mDamage);
        takeDamage(enemy->damage());
    }
}
void Unit::die()
{
    cout << "The " << mType << " is kill." << endl;
}
bool Unit::isDead()
{
    if(mHealth <= 0)
        return true;
    else 
        return false;
}
void Unit::takeDamage(int damage)
{
    if(isDead() == false)
    {
        mHealth = mHealth - damage;
        if(isDead())
        {
            mHealth = 0;
        }
        cout << "The " << mType <<  " took " << damage << " damage ( " << mHealth << " remaining)." << endl;
        if(isDead())
        {
            die();
        }
    }
}