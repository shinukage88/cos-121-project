#ifndef OFFENSIVEFACTORY_H
#define OFFENSIVEFACTORY_H

#include "UnitFactory.h"
#include "Unit.h"
#include "OffensiveKnight.h"
#include "OffensiveMonster.h"

class OffensiveFactory : public UnitFactory
{
	public:
		Unit*createMonster();
		Unit*createKnight();
};

#endif