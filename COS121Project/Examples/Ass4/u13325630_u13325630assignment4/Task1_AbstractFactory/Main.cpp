#include "DefensiveFactory.h"
#include "OffensiveFactory.h"
#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;

int main()
{
	cout << "MONSTER BATTLE II - ARGGGGGHHHHH!" << endl;
	cout << "---------------------------------------------" << endl;

	// For random numbers
	srand(time(NULL));

	DefensiveFactory defensiveFactory;
	OffensiveFactory offensiveFactory;

	// Create the players
	const int UNIT_COUNT = 4;
	Unit* knights[UNIT_COUNT];
	for(int i = 0; i < UNIT_COUNT; ++i)
	{
		if(rand() % 2) knights[i] = offensiveFactory.createKnight();
		else knights[i] = defensiveFactory.createKnight();
	}

	// Create the monsters
	const int MONSTER_COUNT = 20;
	Unit* monsters[MONSTER_COUNT];
	for(int i = 0; i < MONSTER_COUNT; ++i)
	{
		if(rand() % 2) monsters[i] = offensiveFactory.createMonster();
		else monsters[i] = defensiveFactory.createMonster();
	}
	
	// Play
	for(int i = 0 ; i < MONSTER_COUNT; ++i) knights[rand() % UNIT_COUNT]->attack(monsters[i]);

	// Determine winner
	int deadCounter = 0;
	for(int i = 0; i < UNIT_COUNT; ++i)
	{
		if(knights[i]->isDead()) ++deadCounter;
	}
	cout << "---------------------------------------------" << endl;
	if(deadCounter == UNIT_COUNT) cout << "All players were killed by the monsters. You lose!" << endl;
	else cout << "You survived all monsters. You win!" << endl;
	
	// Clear memory
	for(int i = 0; i < UNIT_COUNT; ++i) delete knights[i];
	for(int i = 0; i < MONSTER_COUNT; ++i) delete monsters[i];
	
	return 0;
}
