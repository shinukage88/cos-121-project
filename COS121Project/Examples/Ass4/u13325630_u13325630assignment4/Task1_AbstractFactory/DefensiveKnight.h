#ifndef DEFENSIVEKNIGHT_H
#define DEFENSIVEKNIGHT_H

#include"Knight.h"

class DefensiveKnight : public Knight
{
	public:
		DefensiveKnight();
                void attack(Unit * enemy);
};

#endif