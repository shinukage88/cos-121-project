#include "DefensiveKnight.h"

DefensiveKnight::DefensiveKnight() : Knight("Defensive Knight", 100, 30)
{
	
}
void DefensiveKnight::attack(Unit * enemy)
{
	if(isDead() == false)
	{
        enemy->takeDamage(mDamage);
        takeDamage(enemy->damage());
	}
	if(enemy->isDead() == true)
	{
		mDamage += 1;
	}
}