#ifndef UNIT_H
#define UNIT_H

#include<string>
#include<iostream>
using namespace std;

class Unit
{
	private:
		string mType;
	protected:
		int mHealth;
		int mDamage;
	public:
		Unit(string type, int health, int damage);
		int damage();
		virtual void attack(Unit* enemy);
		void die();
		bool isDead();
		virtual void takeDamage(int damage);
};

#endif