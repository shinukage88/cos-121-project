#ifndef DEFENSIVEFACTORY_H
#define DEFENSIVEFACTORY_H

#include "UnitFactory.h"
#include "Unit.h"
#include "DefensiveKnight.h"
#include "DefensiveMonster.h"

class DefensiveFactory : public UnitFactory
{
	public:
		Unit*createMonster();
		Unit*createKnight();
};

#endif