#include "Monster.h"

Monster::Monster(string type, int health, int damage, int minimum, int maximum): Unit(type, health, damage)
{
    mMinimum = minimum;
    mMaximum = maximum;
}
void Monster::takeDamage(int damage)
{
    int plus = rand()%(mMaximum - mMinimum + 1) + mMinimum;
	Unit::takeDamage(damage+plus);
}
