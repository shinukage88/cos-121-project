#ifndef OFFENSIVEMONSTER_H
#define OFFENSIVEMONSTER_H

#include"Monster.h"

class OffensiveMonster : public Monster
{
	public:
		OffensiveMonster();
                void attack(Unit * enemy);
};

#endif