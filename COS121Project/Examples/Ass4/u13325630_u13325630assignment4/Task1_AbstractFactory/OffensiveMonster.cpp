#include "OffensiveMonster.h"

OffensiveMonster::OffensiveMonster(): Monster("Offensive Monster", 60, 40, 10, 20)
{
	
}
void OffensiveMonster::attack(Unit * enemy)
{
	if(isDead() == false)
	{
        enemy->takeDamage(mDamage);
        takeDamage(enemy->damage());
	}
	if(enemy->isDead() == true)
	{
		mHealth += 2;
	}
}