#include "OffensiveKnight.h"

OffensiveKnight::OffensiveKnight() : Knight("Offensive Knight", 60, 40)
{
	
}
void OffensiveKnight::attack(Unit * enemy)
{
	if(isDead() == false)
	{
        enemy->takeDamage(mDamage);
        takeDamage(enemy->damage());
	}
	if(enemy->isDead() == true)
	{
		mHealth += 1;
	}
}