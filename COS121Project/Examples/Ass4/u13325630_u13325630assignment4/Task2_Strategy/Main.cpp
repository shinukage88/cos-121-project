#include "SortEngine.h"
#include <time.h>
#include <stdlib.h>

using namespace std;

int main()
{
	// Set seed for random number generation
	srand(time(NULL));

	const int SIZE = 10;
	int values1[SIZE], values2[SIZE];

	for(int i = 0; i < SIZE; ++i)
	{
		// Generate two numbers between 1 and 100
		values1[i] = rand() % 100 + 1;
		values2[i] = rand() % 100 + 1;
	}

	SortEngine engine;

	// Sort with the default sorting strategy
	engine.sort(values1, SIZE);
	
	// Change the sorting strategy
	engine.changeStrategy();
	engine.sort(values2, SIZE);
	
	return 0;
}
