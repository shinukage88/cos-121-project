#ifndef SORTENGINE_H
#define SORTENGINE_H

#include "Sorter.h"
#include "BubbleSorter.h"
#include "InsertionSorter.h"

using namespace std;

class SortEngine
{
    private:
        Sorter * mSorter;
        
	void print(int values[] , int size);
    
    public:
        SortEngine();
        ~SortEngine();
        void sort(int values[] , int size);
        void changeStrategy();    
};

#endif