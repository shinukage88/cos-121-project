#include "InsertionSorter.h"

#include <iostream>
void InsertionSorter::sort(int values[], int size)
{
    for (int i = 1; i < size; i++)
	{
                int temp;
                int j = i;
		
                while (j > 0 && values[j] < values[j - 1])
                    {
                            temp = values[j];
                            values[j] = values[j - 1];
                            values[j - 1] = temp;
                            j--;
                    }
	}
}
