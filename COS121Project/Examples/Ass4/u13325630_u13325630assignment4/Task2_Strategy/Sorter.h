#ifndef SORTER_H
#define SORTER_H

#include <string>

using namespace std;

class Sorter
{
    public:
        virtual void sort(int values[] , int  size) = 0;
};

#endif