#ifndef INSERTIONSORTER_H
#define INSERTIONSORTER_H

#include "Sorter.h"
#include <string>

using namespace std;

class InsertionSorter : public Sorter
{
    private:
        string name;
    public:
        void sort(int values[] , int size);
};

#endif