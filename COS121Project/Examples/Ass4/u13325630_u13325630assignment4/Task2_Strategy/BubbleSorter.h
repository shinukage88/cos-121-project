#ifndef BUBBLESORTER_H
#define BUBBLESORTER_H

#include "Sorter.h"
#include <string>

using namespace std;

class BubbleSorter: public Sorter
{
    private:
        string name;
    public:
        void sort(int values[] , int size);
};

#endif