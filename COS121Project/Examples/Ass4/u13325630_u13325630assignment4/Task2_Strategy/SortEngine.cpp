#include "SortEngine.h"
#include <iostream>

SortEngine::SortEngine()
{
    
	mSorter = new BubbleSorter();
	cout << "Using Bubble Sort" << endl;
}

SortEngine::~SortEngine()
{
	delete mSorter;
}

void SortEngine::sort(int values[], int size)
{
	cout << "Unsorted:     [";
	for(int i = 0; i < size; i++)
		{
			if (i == size - 1)
			{
                    cout << values[i];
			}
			else
			{
				cout << values[i] << ", ";
			}
		}
	cout << "]" <<endl;
	mSorter->sort(values, size);
	print(values, size);
}

void SortEngine::changeStrategy()
{
	Sorter * bubble = new BubbleSorter();
	Sorter * insertion = new InsertionSorter();
	BubbleSorter* bs;
	InsertionSorter* is;
	bs = dynamic_cast<BubbleSorter*>(bubble);
	is = dynamic_cast<InsertionSorter*>(insertion);
	
	if (bs == 0)
	{
		mSorter = new InsertionSorter();
		cout << "Using Insertion Sort" <<endl;
	}
	else if(is == 0)
	{
            mSorter = new BubbleSorter();
            cout << "Using Bubble Sort" <<endl;
        } 
}

void SortEngine::print(int values[], int size)
{
	cout << "Sorted:       [";
	for(int i = 0; i < size; i++)
        {
                if (i == size - 1)
                {
                    cout << values[i];
                }
                else
                {
                        cout << values[i] << ", ";
                }
        }
    cout << "]" << endl;
}